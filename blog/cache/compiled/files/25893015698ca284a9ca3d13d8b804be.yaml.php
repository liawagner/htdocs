<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/christian/developer/webserver/htdocs/soraarticle/user/accounts/chriswayg.yaml',
    'modified' => 1523027541,
    'data' => [
        'email' => 'chriswayg@gmail.com',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'fullname' => 'Christian Wagner',
        'title' => 'Mr.',
        'state' => 'enabled',
        'hashed_password' => '$2y$10$ud1YHDwhyaZjztprjtprLeKC2.8zrPDVgkOQLQpjpF5sdunxRiK7O'
    ]
];
